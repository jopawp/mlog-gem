mlog-gem
========

Matt's console logging Ruby gem with colorized output.

Contributors
------------

Matt Wiedner <jopawp@gmail.com>

Requirements
------------

Requires the Linux ncurses package to be installed. mlog is dependent upon the
"tput" utility provided by ncurses for colorized output.

Excepted from the tput man page:

```
The  tput  utility  uses  the terminfo database to make the values of terminal-
dependent capabilities and information available to the shell.
```

It's intended use is to make ncurses capabilties accessible from a shell
script. But it work beautifully for colorizing Ruby console output too and
it does so with proper termcap support.

mlog provides support for alternate output streams other than STDOUT. This has
not been well tested.

Usage
-----

Excerpted from test/mlog-test.rb


```
require 'mlog'

log = Log.new

# Log debug messages
log.debug_flag = true
puts "1. Displaying all possible color combinations:"
log.test "TEST"

puts "\n2. Testing convenience functions: info, success, error, debug"
log.info "This is an info message."
log.success "This is a success message."
log.error "This is an error message."
log.debug "This is a debug message."

puts "\n3. Changing default colors..."
log.info_color = log.cyan + log.standout
log.success_color = log.green + log.standout
log.error_color = log.red + log.standout
log.debug_color = log.yellow + log.standout
log.info "This is an info message."
log.success "This is a success message."
log.error "This is an error message."
log.debug "This is a debug message."

log.debug_flag = false
puts "\n4. Debug messages suppressed. No yellow standout debug message should appear on the next line..."
log.debug "This is a debug message."
puts "No yellow standout debug message should appear on the previous line."
puts "\nEND"
```

To do
----

The tput utility also allows for cursor positioning. Perhaps someday I will
convert this into a full terminal control gem.