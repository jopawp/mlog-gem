Gem::Specification.new do |s|
	s.name        = 'mlog'
	s.version     = '0.3.0'
	s.date        = '2015-05-22'
	s.summary     = "Terminal console logging"
	s.description = "Terminal console logging with color support provided by tput gem.."
	s.authors     = ["Matt Weidner"]
	s.email       = 'jopawp@gmail.com'
	s.files       = ["lib/mlog.rb"]
	s.homepage    =
		'http://rubygems.org/gems/hola'
	s.license       = 'LGPL'
end
