class Log
	attr_accessor :output_stream, :nocolor, :debug_flag, :debug_color, 
					:error_color, :success_color, :info_color

	attr_reader :bold, :blink, :underline, :nounderline, :standout, :nostandout,
				:normal, :black, :red, :green, :yellow, :blue, :magenta, :cyan,
				:white, :bgblack, :bgred, :bggreen, :bgyellow, :bgblue,
				:bgmagenta, :bgcyan, :bgwhite

	def initialize
		@output_stream = STDOUT
		@nocolor = false
		@debug_flag = false

		# tput color sequences
		@bold = `tput bold`
		@blink = `tput blink`
		@underline = `tput smul`
		@nounderline = `tput rmul`
		@standout = `tput smso`
		@nostandout = `tput rmso`
		@normal = `tput sgr0`
		@black = `tput setaf 0`
		@red = `tput setaf 1`
		@green = `tput setaf 2`
		@yellow = `tput setaf 3`
		@blue = `tput setaf 4`
		@magenta = `tput setaf 5`
		@cyan = `tput setaf 6`
		@white = `tput setaf 7`
		@bgblack = `tput setab 0`
		@bgred = `tput setab 1`
		@bggreen = `tput setab 2`
		@bgyellow = `tput setab 3`
		@bgblue = `tput setab 4`
		@bgmagenta = `tput setab 5`
		@bgcyan = `tput setab 6`
		@bgwhite = `tput setab 7`

		@success_color = green + bold
		@error_color = red + bold
		@info_color = cyan + bold
		@debug_color = magenta + standout
	end

	def color(text=nil, color=normal)
		# Only print the color code if the output device is a console AND
		# color output has not been disabled.  # This prevent color codes from
		# being printed when output is redirected to a file or the user has
		# explicitly disabled colorized output.
		if STDOUT.tty?
 			unless @nocolor
				print color 
			end
		end
		
		# If the user only passes in a color code, do not try to print
		# any text.
		print text unless text == nil || text == ""
		
		# Reset the terminal to normal output if a color code was output
		# earlier.
		if STDOUT.tty? 
			unless @color
				print normal
			end
		end
	end

	def error(text)
		text = "[-] " + text
		output_stream.puts color(text, error_color)
	end

	def success(text)
		text = "[+] " + text
		output_stream.puts color(text, success_color)
	end

	def info(text)
		text = "[I] " + text
		output_stream.puts color(text, info_color)
	end

	def debug(text)
		return unless debug_flag
		text = "[D] " + text
		output_stream.puts color(text, debug_color)
	end

	def test(text)
		special = [
			bold,
			blink,
			underline,
			nounderline,
			standout,
			nostandout,
			normal
		]

		fgcolor = [
			black,
			red,
			green,
			yellow,
			blue,
			magenta,
			cyan,
			white
		]

		bgcolor = [
			bgblack,
			bgred,
			bggreen,
			bgyellow,
			bgblue,
			bgmagenta,
			bgcyan,
			bgwhite
		]

		i = 0
		bgcolor.each do |sp|
			fgcolor.each do |fg|
				special.reverse_each do |bg|
					print color(text.center(text.length + text.length/2), sp + fg + bg)
				end
				print "\n"
			end
		end
		print color("", normal)
	end
end
