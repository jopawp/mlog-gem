#!/usr/bin/ruby

require 'mlog'

log = Log.new

# Log debug messages
log.debug_flag = true
puts "1. Displaying all possible color combinations:"
log.test "TEST"
puts "\n2. Testing convenience functions: info, success, error, debug"
log.info "This is an info message."
log.success "This is a success message."
log.error "This is an error message."
log.debug "This is a debug message."
puts "\n3. Changing default colors..."
log.info_color = log.cyan + log.standout
log.success_color = log.green + log.standout
log.error_color = log.red + log.standout
log.debug_color = log.yellow + log.standout
log.info "This is an info message."
log.success "This is a success message."
log.error "This is an error message."
log.debug "This is a debug message."
log.debug_flag = false
puts "\n4. Debug messages suppressed. No yellow debug message should appear on the next line..."
log.debug "This is a debug message."
puts "No yellow standout debug message should appear on the previous line."
puts "\nEND"

